package aditz.parser;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
public abstract class Function implements Expression {
    protected Expression argument;
    protected Function(Expression argument) {
        this.argument=argument;
    }
    public Function() {}

    @Override
    public Associative getAssociative() {
        return Associative.RIGHT;
    }

    @Override
    public int priority() {
        return 4;
    }

    public void bind(Expression argument) {
        this.argument=argument;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName().toLowerCase();
    }
}

class Sin extends Function {
    public Sin(Expression argument) {
        super(argument);
    }
    public Sin() {}
    @Override
    public double evaluate() {
        return Math.sin(argument.evaluate());  //To change body of implemented methods use File | Settings | File Templates.
    }
}

class Cos extends Function {
    public Cos(Expression argument) {
        super(argument);
    }
    public Cos() {}
    @Override
    public double evaluate() {
        return Math.cos(argument.evaluate());  //To change body of implemented methods use File | Settings | File Templates.
    }
}

//TODO: moar