package aditz.parser;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */

import java.util.HashMap;
import java.util.Map;

/**
 * Will create Expressions based on name
 */
public class ExpressionFactory {
    private Map<String, Class<? extends Expression>> symbols = new HashMap<>();

    //TODO: add exceptions here
    public void addExpression(String regex, Class<? extends Expression> type) {
        if(regex==null || type==null || regex.equals(""))
            return;
        if(symbols.containsKey(regex))
            return;
        symbols.put(regex, type);
    }

    public Expression createExpression(String regex) {
        Class<? extends Expression> type= symbols.get(regex);
        if(type!=null) {
            try {
                return type.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
