package aditz.parser;

import aditz.logic.fitness.Fitness;
import aditz.parser.exceptions.ArgumentsException;
import aditz.parser.exceptions.ParserException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
public class Parser implements Fitness {
    private ExpressionFactory expressionFactory=new ExpressionFactory();
    private VariablePool variablePool=new VariablePool();
    private Tokenizer tokenizer=new Tokenizer(expressionFactory,variablePool);
    private Verificator verificator=new Verificator();
    private Expression solved;

    public Parser() {
        //VARIABLE FORMAT
        Tokenizer.setConstantRegex("^\\d+(\\.\\d+)?");
        Tokenizer.setVariableRegex("^[A-Za-z_]+");

        //BINARY OPERATORS
        addSymbol("^\\+", Add.class);
        addSymbol("^-",Sub.class);
        addSymbol("^\\^", Pow.class);
        addSymbol("^\\*", Mul.class);
        addSymbol("^/", Div.class);

        //PARENTHESES
        addSymbol("^\\)", RightParen.class);
        addSymbol("^\\(", LeftParen.class);

        //FUNCTIONS
        addSymbol("^sin", Sin.class);
        addSymbol("^cos", Cos.class);

        //CONSTANTS
        addSymbol("^(e|E)", E.class);
        addSymbol("^([Pp][Ii])", Pi.class);
    }

	public VariablePool getVariablePool() {
		return variablePool;
	}

	@Override
	public Parser getParser() {
		return null;
	}

	public void parse(String expression) throws ParserException {
        expression=expression.replaceAll("\\s",""); //get rid of white characters
        expression=expression.replaceAll("\\(-","\\(0-"); //helps with unary minus
        if(expression.startsWith("-"))  //helps with unary minus
            expression="0"+expression;

        List<Expression> OOexpression = tokenizer.strip(expression);
        verificator.verifySyntax(OOexpression);

        List<Expression> postfix=infixToRPN(OOexpression);
        solved=solveRPN(postfix);
    }

    public double eval(double... val) throws ParserException{
        if(solved==null)
            throw new RuntimeException("Nothing was parsed");
        variablePool.fill(val);
        return solved.evaluate();
    }

    private void addSymbol(String regex, Class<? extends Expression> type) {
        tokenizer.addSymbol(regex);
        expressionFactory.addExpression(regex,type);
    }

    private List<Expression> infixToRPN(List<Expression> infix) {
        ArrayList<Expression> output=new ArrayList<Expression>();
        LinkedList<Expression> opStack=new LinkedList<Expression>();

        for(Expression expression: infix) {
            if((expression instanceof Operator) || (expression instanceof Function)) {
                while(!opStack.isEmpty() &&
                        (opStack.peek() instanceof Operator) || (opStack.peek() instanceof Function)) {
                    if((expression.priority()<=opStack.peek().priority() && expression.getAssociative()==Associative.LEFT) ||
                       (expression.priority()<opStack.peek().priority() && expression.getAssociative()==Associative.RIGHT))
                        output.add(opStack.pop());
                    else
                        break;
                }
                opStack.push(expression);
            }
            else if(expression instanceof LeftParen) {
                opStack.push(expression);
            }
            else if(expression instanceof RightParen) {
                while(!opStack.isEmpty() && !(opStack.peek() instanceof LeftParen))
                    output.add(opStack.pop());
                if(!opStack.isEmpty())
                    opStack.pop();
            }
            else {
                output.add(expression);
            }
        }
        while(!opStack.isEmpty()) {
            output.add(opStack.pop());
        }
        return output;
    }

    private Expression solveRPN(List<Expression> expression) throws ArgumentsException {
        LinkedList<Expression> stack=new LinkedList<>();
        for(Expression exp: expression) {
            if(exp instanceof Number) {
                stack.push(exp);
            }
            else if(exp instanceof Operator) {
                if(stack.size()<2)
                    throw new ArgumentsException("Missing argument for: "+exp.toString());
                Expression ex1=stack.pop();
                Expression ex2=stack.pop();
                ((Operator)exp).bind(ex2,ex1);
                stack.push(exp);
            }
            else if(exp instanceof Function) {
                if(stack.size()<1)
                    throw new ArgumentsException("Missing argument for: "+exp.toString());
                Expression ex1=stack.pop();
                ((Function)exp).bind(ex1);
                stack.push(exp);
            }
        }
        if(stack.size()!=1)
            throw new ArgumentsException("Too many values");
        return stack.pop();
    }

    @Override
    public double fitnessFunc(double... d) {
        try {
            return eval(d);
        } catch (ParserException e) {
            e.printStackTrace();
        }
        return Double.NaN;
    }
}
