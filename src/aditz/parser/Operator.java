package aditz.parser;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 16:08
 * To change this template use File | Settings | File Templates.
 */
public abstract class Operator implements Expression {
    protected Expression left;
    protected Expression right;
    protected Operator(Expression left, Expression right) {
        this.left=left;
        this.right=right;
    }
    public Operator() {}
    public void bind(Expression left, Expression right) {
        this.left=left;
        this.right=right;
    }
    @Override
    public Associative getAssociative() {
        return Associative.LEFT;
    }
}

class Add extends Operator {
    public Add(Expression left, Expression right) {
       super(left,right);
    }
    public Add() {}

    @Override
    public int priority() {
        return 1;
    }
    @Override
    public double evaluate() {
        return left.evaluate()+right.evaluate();
    }

    @Override
    public String toString() {
        return "+";
    }
}

class Sub extends Operator {
    public Sub(Expression left, Expression right) {
        super(left,right);
    }
    public Sub() {}

    @Override
    public int priority() {
        return 1;
    }
    @Override
    public double evaluate() {
        return left.evaluate()-right.evaluate();
    }
    @Override
    public String toString() {
        return "-";
    }
}

class Mul extends Operator {
    public Mul(Expression left, Expression right) {
        super(left,right);
    }
    public Mul() {}

    @Override
    public int priority() {
        return 2;
    }
    @Override
    public double evaluate() {
        return left.evaluate()*right.evaluate();
    }
    @Override
    public String toString() {
        return "*";
    }
}

class Div extends Operator {
    public Div(Expression left, Expression right) {
        super(left,right);
    }
    public Div() {}
    @Override
    public int priority() {
        return 2;
    }
    //TODO: div by zero handling
    @Override
    public double evaluate() {
        return left.evaluate()/right.evaluate();
    }
    @Override
    public String toString() {
        return "/";
    }
}

class Pow extends Operator {
    public Pow(Expression left, Expression right) {
        super(left,right);
    }
    @Override
    public int priority() {
        return 3;
    }
    public Pow() {}
    @Override
    public double evaluate() {
        return Math.pow(left.evaluate(),right.evaluate());
    }
    @Override
    public Associative getAssociative() {
        return Associative.RIGHT;
    }
    @Override
    public String toString() {
        return "^";
    }
}

