package aditz.parser;

import aditz.parser.exceptions.UnknownSymbolException;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public class Tokenizer {
    private static String variable ="^[A-Za-z_]+";
    private static String constant ="^[0-9]+\\.?[0-9]*";

    public static void setVariableRegex(String regex) {
        variable =regex;
    }

    public static void setConstantRegex(String regex) {
        constant =regex;
    }

    private Map<String, Pattern> symbols = new HashMap<>();
    private ExpressionFactory expressionFactory;
    private VariablePool variablePool;

    public Tokenizer(ExpressionFactory factory, VariablePool pool) {
        expressionFactory=factory;
        variablePool=pool;
    }

    public void addSymbol(String regex) {
        if(regex==null || regex.equals(""))
            return;
        if(symbols.containsKey(regex))
            return;
        symbols.put(regex, Pattern.compile(regex));
    }


    public List<Expression> strip(String expression) throws UnknownSymbolException {
        Matcher matcher;
        List<Expression> OOexpression=new ArrayList<>();
        out:
        while(expression.length()!=0) {
            for(Map.Entry<String, Pattern> symbol: symbols.entrySet()) {
                matcher=symbol.getValue().matcher(expression);
                if(matcher.find())  {
                    OOexpression.add(expressionFactory.createExpression(symbol.getKey()));
                    expression=expression.substring(matcher.end());
                    continue out;
                }
            }
            //if it doesn't match anything than it is variable
            matcher=Pattern.compile(variable).matcher(expression);
            if(matcher.find())  {
                OOexpression.add(variablePool.getVariable(matcher.group()));
                expression=expression.substring(matcher.end());
                continue;
            }

            matcher=Pattern.compile(constant).matcher(expression);
            if(matcher.find())  {
                OOexpression.add(new Constant(Double.parseDouble(matcher.group())));
                expression=expression.substring(matcher.end());
                continue;
            }
            throw new UnknownSymbolException("Unexpected symbol at:\n"+expression);
        }
        return OOexpression;
    }

}
