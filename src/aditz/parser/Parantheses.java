package aditz.parser;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 17:01
 * To change this template use File | Settings | File Templates.
 */

class LeftParen implements Expression {
    @Override
    public int priority() {
        return 5;
    }

    @Override
    public Associative getAssociative() {
        return null;
    }

    @Override
    public double evaluate() {
        return Double.NaN;
    }

    @Override
    public String toString() {
        return "(";
    }
}

class RightParen implements Expression {
    @Override
    public int priority() {
        return 5;
    }
    @Override
    public double evaluate() {
        return Double.NaN;
    }

    @Override
    public Associative getAssociative() {
        return null;
    }

    @Override
    public String toString() {
        return ")";
    }
}
