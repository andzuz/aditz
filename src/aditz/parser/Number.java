package aditz.parser;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
public abstract class Number implements Expression {
    protected double value;
    @Override
    public int priority() {
        return 0;
    }
    @Override
    public Associative getAssociative() {
        return Associative.RIGHT;
    }
}

class Constant extends Number {
    public Constant(double value) {
        this.value=value;
    }
    @Override
    public double evaluate() {
        return value;
    }

    @Override
    public String toString() {
        return Double.toString(value);
    }
}

class E extends Constant {
    public E() {
        super(Math.E);
    }
}

class Pi extends Constant {
    public Pi() {
        super(Math.PI);
    }
}

