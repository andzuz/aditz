package aditz.parser;

/**
 * Created by: Piotrek
 * Date: 27.05.13
 * Time: 15:24
 */

public class Variable extends Number {
	private String name;
	public Variable(String name) {
		this.name=name;
	}
	public void setValue(double value) {
		this.value=value;
	}
	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if((obj instanceof Variable))
			return name.equals(((Variable)obj).name);
		return false;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public double evaluate() {
		return value;
	}
	@Override
	public String toString() {
		return name;
	}
}