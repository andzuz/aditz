package aditz.parser;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
enum Associative { RIGHT , LEFT }

public interface Expression {
    double evaluate();
    int priority();
    Associative getAssociative();
}
