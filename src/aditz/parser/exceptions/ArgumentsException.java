package aditz.parser.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 17.03.13
 * Time: 12:38
 * To change this template use File | Settings | File Templates.
 */
public class ArgumentsException extends ParserException{
    public ArgumentsException(String msg) {
        super(msg);
    }
}
