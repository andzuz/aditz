package aditz.parser.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 17.03.13
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */
public class UnknownSymbolException extends ParserException{
    public UnknownSymbolException(String msg) {
        super(msg);
    }
}
