package aditz.parser;

import aditz.parser.exceptions.ParserException;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 16.03.13
 * Time: 18:17
 * To change this template use File | Settings | File Templates.
 */
public class VariablePool {
    private List<Variable> pool=new ArrayList<>();

    public void fill(double... values) throws ParserException{
        if(pool.size()!=values.length)
            throw new ParserException("Not enough data");
        int index=0;
        for(Variable var: pool)
            var.setValue(values[index++]);
    }

    public Variable getVariable(String name) {
        for(Variable var: pool) {
            if(var.getName().equals(name))
                return var;
        }
        Variable var=new Variable(name);
        pool.add(var);
        return var;
    }

	public List<Variable> getPool() {
		return pool;
	}
}
