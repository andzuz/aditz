package aditz.parser;

import aditz.parser.exceptions.SyntaxException;

import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 17.03.13
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */
public class Verificator {
    enum Type {Number, Function, Operator, LeftParen, RightParen}
    static boolean[][] allowedCombination = {
            {false, false, true, false,true},
            {true, true, false, true, false},
            {true, true, false, true, false},
            {true, true, false, true, false},
            {false, false, true, false, true},
    };

    public void verifySyntax(List<Expression> expressionList) throws SyntaxException {
        int index=0;
        int parenCter=0;
        Type previous, next;
        Iterator<Expression> iter=expressionList.iterator();
        if(!iter.hasNext())
            throwFormattedException(expressionList, index,"Empty expression");
        previous=getType(iter.next());
        if(previous==Type.Operator)
            throwFormattedException(expressionList,index,"Expression cannot start with operator");

        while (iter.hasNext()) {
            if(previous==Type.LeftParen)
                parenCter++;
            else if(previous==Type.RightParen) {
                parenCter--;
                if(parenCter<0) throwFormattedException(expressionList,index,"Too many )");
            }

            index++;
            next=getType(iter.next());
            if(!allowedCombination[previous.ordinal()][next.ordinal()]) {
                String msg=previous.name()+" cannot precede "+next.name();
                throwFormattedException(expressionList,index,msg);
            }
            previous=next;
        }
        if(previous==Type.LeftParen)
            throwFormattedException(expressionList, index,"Cannot end with (");
        else if(previous==Type.RightParen)
            parenCter--;
        if(parenCter!=0)
            throwFormattedException(expressionList, index,"Parentheses doesn't match");

    }

    private void throwFormattedException(List<Expression> expressionList, int index, String msg) throws SyntaxException {
        int letters=0;
        StringBuilder sb= new StringBuilder();
        Iterator<Expression> iter=expressionList.iterator();
        while(iter.hasNext()) {
            String s=iter.next().toString();
            sb.append(s);
            if(index>0)
                letters+=s.length();
            index--;
        }
        sb.append("\n");
        for(int i=0; i<letters; i++)
            sb.append(" ");
        sb.append("^\n");
        sb.append(msg);
        String result=sb.toString();
        //result=result.replaceAll("\\(0-","\\(-");
        //if(result.startsWith("0-"))
            //result=result.substring(1);
        throw  new SyntaxException(result);
    }

    private Type getType(Expression expression) {
        if(expression instanceof Operator)
            return Type.Operator;
        if(expression instanceof Function)
            return Type.Function;
        if(expression instanceof Number)
            return Type.Number;
        if(expression instanceof LeftParen)
            return Type.LeftParen;
        if(expression instanceof RightParen)
            return Type.RightParen;
        return null;
    }
}
