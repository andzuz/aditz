package aditz.GUI;

import aditz.logic.fitness.MaximumSeeking;
import aditz.parser.Parser;
import aditz.parser.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:29
 */

class HeaderPanel extends JPanel {
	final JLabel headerLabel = new JLabel("Steering console");
	final JLabel functionLabel = new JLabel("Function: ");
	final JTextField functionText = new JTextField("-(z+38)*(z+26)*(z-21)*(z+5)*(5*z-10)*(2*z+3)*(z-30)*(z-40)*(1/50000000000)");

	final Panel panel;
	RangesPanel rp;

	HeaderPanel(Panel p) {
		super(new GridBagLayout());

		this.panel = p;
		setBackground(Color.WHITE);
		panel.fit = new MaximumSeeking(functionText.getText());

		headerLabel.setFont(new Font(headerLabel.getFont().getPSName(), Font.PLAIN, 24));

		add(headerLabel, new GBC(0, 0, 4, 1).setWeight(100, 0).setIpad(0, 15).setAnchor(GBC.NORTH));
		add(functionLabel, new GBC(1, 1).setFill(GBC.BOTH).setAllInsets(5));
		add(functionText, new GBC(2, 1, 3, 1).setFill(GBC.BOTH).setWeight(100, 0).setIpad(0, 10).setAnchor(GBC.NORTH).setAllInsets(5));

		functionText.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.fit = new MaximumSeeking(functionText.getText());
				Parser p = panel.fit.getParser();

				switch(p.getVariablePool().getPool().size()) {
					case 1:
						panel.type = Type.TwoD;
						break;
					case 2:
						panel.type = Type.ThreeD;
						break;
					default:
						panel.type = Type.WhoKnows;
						break;
				}

				if(rp!=null) {
					remove(rp);
					rp.setVisible(false);
				}

				continueAdding(p.getVariablePool().getPool());

				functionText.setEnabled(false);
			}
		});
	}

	void continueAdding(java.util.List<Variable> v) {
		java.util.List<String> s = new LinkedList<>();

		for(Variable i : v)
			s.add(i.getName());

		rp = new RangesPanel(s, panel);

		add(rp, new GBC(0, 3, 5, 1).setFill(GBC.BOTH).setWeight(100,100));
		revalidate();
	}
}