package aditz.GUI;

import aditz.logic.algorithm.Algorithm;
import aditz.logic.fitness.Fitness;

import javax.swing.*;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:27
 */

class Panel extends JPanel {
	JPanel charts = new JPanel(new GridLayout(0,1));
	HeaderPanel hp = new HeaderPanel(this);

	Charts2D chart2D = new Charts2D(this);
	Chart3D chart3D = new Chart3D();

	Fitness fit;
	Algorithm algo = new Algorithm();
	Type type;

	Panel() {
		super(new GridLayout(1,0));

		charts.add(chart2D);
		setBackground(Color.WHITE);

		add(hp);
		add(charts);
	}

	public void dispose() {
		algo = new Algorithm();

		remove(charts);
		charts.setVisible(false);
		remove(hp);
		hp.setVisible(false);

		charts = new JPanel(new GridLayout(0,1));
		hp = new HeaderPanel(this);
		chart2D = new Charts2D(this);
		chart3D = new Chart3D();

		fit = null;
		type = null;

		charts.add(chart2D);

		add(hp);
		add(charts);

		revalidate();
	}
}