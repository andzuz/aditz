package aditz.GUI;

import aditz.logic.fitness.Fitness;
import aditz.logic.fitness.MinimumSeeking;
import aditz.logic.point.*;
import aditz.logic.population.PopulationStats;
import org.jzy3d.chart.Chart;
import org.jzy3d.chart.controllers.mouse.camera.CameraMouseController;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.builder.concrete.OrthonormalGrid;
import org.jzy3d.plot3d.primitives.AbstractDrawable;
import org.jzy3d.plot3d.primitives.LineStrip;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.jzy3d.plot3d.rendering.view.modes.ViewBoundMode;

import javax.swing.*;
import java.awt.*;
import java.awt.Point;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:28
 */

class Chart3D extends JFrame {
	private static final int steps=50;
	private static final float width=7;

	private Chart chart;
	private org.jzy3d.plot3d.primitives.Shape surface;
	private List<LineStrip> list = new LinkedList<>();
	private Fitness fitness;
	private Range rangeX, rangeY;

	public Chart3D() {
		chart = new Chart(Quality.Advanced);
		chart.addController(new CameraMouseController());

		add((Component) chart.getCanvas());
	}

	public Component getComp() {
		return (Component) chart.getCanvas();
	}

	public void setFitness(Fitness fitness) {
		this.fitness = fitness;
	}

	public void setRange(aditz.logic.point.Point min, aditz.logic.point.Point max) {
		rangeX=new Range(min.getVal()[0], max.getVal()[0]);
		rangeY=new Range(min.getVal()[1], max.getVal()[1]);
	}

	private void clearPrevious() {
		clearPoints();
		if(surface!=null) {
			chart.getScene().getGraph().remove(surface, false);
			surface.dispose();
		}
	}

	private void clearPoints() {
		if(!list.isEmpty()) {
			for(AbstractDrawable drawable: list) {
				chart.getScene().getGraph().remove(drawable,false);
				drawable.dispose();
			}
			list.clear();
		}
	}

	public void initialize() {
		clearPrevious();            //clear chart

		Mapper mapper = new Mapper() {
			@Override
			public double f(double x, double y) {
				if(fitness instanceof MinimumSeeking)
					return -fitness.fitnessFunc(new double[]{x,y});
				else
					return  fitness.fitnessFunc(new double[]{x,y});
			}
		};
		surface= Builder.buildOrthonormal(new OrthonormalGrid(rangeX, steps, rangeY, steps), mapper);
		surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(),
				surface.getBounds().getZmax(), new org.jzy3d.colors.Color(1,1,1,.5f)));
		surface.setFaceDisplayed(true);
		surface.setWireframeDisplayed(false);
		surface.setWireframeColor(org.jzy3d.colors.Color.BLACK);

		chart.getScene().getGraph().add(surface, true);     //add new graph
		chart.getView().setMaximized(true);
		chart.getCanvas().getView().setBoundMode(ViewBoundMode.AUTO_FIT);
	}

	public void updatePopulation(PopulationStats population) {
		clearPoints();
		final Random rng = new Random();
		for(aditz.logic.point.Point p: population.getAll()) {
			LineStrip line=new LineStrip();
			line.add(new org.jzy3d.plot3d.primitives.Point(
					new Coord3d(p.getVal()[0],p.getVal()[1], chart.getScene().getGraph().getBounds().getZmin()),
					org.jzy3d.colors.Color.GREEN, width));
			line.add(new org.jzy3d.plot3d.primitives.Point(
					new Coord3d(p.getVal()[0] ,p.getVal()[1], chart.getScene().getGraph().getBounds().getZmax()),
					org.jzy3d.colors.Color.GREEN, width));

			list.add(line);
		}
		aditz.logic.point.Point p=population.getFittest();
		LineStrip line=new LineStrip();
		line.add(new org.jzy3d.plot3d.primitives.Point(
				new Coord3d(p.getVal()[0],p.getVal()[1], chart.getScene().getGraph().getBounds().getZmin()),
				org.jzy3d.colors.Color.RED, width));
		line.add(new org.jzy3d.plot3d.primitives.Point(
				new Coord3d(p.getVal()[0] ,p.getVal()[1], chart.getScene().getGraph().getBounds().getZmax()),
				org.jzy3d.colors.Color.RED, width));

		list.add(line);
		chart.getScene().getGraph().add(list,false);

	}

	public void dispose() {
		clearPrevious();
		chart.dispose();
	}
}