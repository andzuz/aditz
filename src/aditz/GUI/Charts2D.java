package aditz.GUI;

import aditz.logic.algorithm.AlgorithmProgressListener;
import aditz.logic.fitness.Fitness;
import aditz.logic.population.PopulationStats;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.function.Function2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:28
 */

class Charts2D extends JPanel implements AlgorithmProgressListener {
	private TimeSeries bestSolutionSeries = new TimeSeries("Best solution");
	private TimeSeries worstSolutionSeries = new TimeSeries("Worst solution");
	private TimeSeries meanSolutionSeries = new TimeSeries("Mean solution");

	private XYSeries populationSeries;
	private XYSeries bestOfPopulationSeries;
	private XYDataset functionDataset;

	private long now = new Date().getTime();

	Fitness fit;
	Panel p;
	int delay = 350;
	Type type;
	JTextArea console;
	String result = "";
	double min;
	double max;
	JButton startButton;

	public Charts2D(Panel p) {
		super(new GridLayout(0,1));

		this.p = p;

		final JFreeChart chart = createFitnessChart(new TimeSeriesCollection(bestSolutionSeries), new TimeSeriesCollection(meanSolutionSeries), new TimeSeriesCollection(worstSolutionSeries));
		final ChartPanel chartPanel = new ChartPanel(chart, true);

		add(chartPanel);
		setVisible(true);
	}

	private JFreeChart createFitnessChart(final XYDataset bestDataset, final XYDataset meanDataset, final XYDataset worstDataset) {
		final JFreeChart result = ChartFactory.createTimeSeriesChart(
				"Solution",
				"Time",
				"Solution",
				bestDataset,
				true,
				true,
				false
		);

		XYPlot plot = (XYPlot) result.getPlot();

		plot.setDataset(1, meanDataset);
		plot.setDataset(2, worstDataset);

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setAutoRange(true);
		axis.setDateFormatOverride(new SimpleDateFormat("m:s.S"));

		plot.setBackgroundPaint(new GradientPaint(0 ,0, new Color(0xFFFFFF), 0, 1, new Color(-5318657)));

		NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
		yAxis.setAutoRangeIncludesZero(true);

		XYItemRenderer renderer0 = plot.getRenderer();
		renderer0.setSeriesPaint(0, new Color(-16735550));

		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(true, false);
		renderer1.setSeriesPaint(0, new Color(-16766231));
		plot.setRenderer(1, renderer1);

		XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, false);
		renderer2.setSeriesPaint(0, new Color(-7470854));
		plot.setRenderer(2, renderer2);

		return result;
	}

	JFreeChart createFunctionChart(XYDataset bestOfPolulation, XYDataset population, XYDataset function) {

		JFreeChart chart = ChartFactory.createXYLineChart(
				"Population stats",
				"X",
				"Y",
				bestOfPolulation,
				PlotOrientation.VERTICAL,
				true,
				true,
				false
		);

		XYPlot plot = (XYPlot) chart.getPlot();

		plot.setDataset(1, population);
		plot.setDataset(2, function);

		plot.setBackgroundPaint(Color.white);
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		plot.setDomainGridlinePaint(Color.lightGray);
		plot.setRangeGridlinePaint(Color.lightGray);

		XYLineAndShapeRenderer renderer0 = (XYLineAndShapeRenderer) plot.getRenderer();
		plot.setRenderer(0, renderer0);
		renderer0.setBaseShapesVisible(true);
		renderer0.setBaseLinesVisible(false);

		XYLineAndShapeRenderer renderer1 = new XYLineAndShapeRenderer(false, true);
		plot.setRenderer(1, renderer1);

		XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, false);
		plot.setRenderer(2, renderer2);
		renderer2.setSeriesPaint(0, Color.BLACK);

		return chart;
	}

	public void newGenerationCreated(final PopulationStats population) {
		final String pt= Arrays.toString(population.getFittest().getVal());
		System.out.println(pt+" "+population.getMaxFitness());
		result = pt+" "+population.getMaxFitness();

		// update wykresu liczebności populacji
		long current = new Date().getTime()- now;

		bestSolutionSeries.addOrUpdate(new FixedMillisecond(current), population.getMaxFitness());
		worstSolutionSeries.addOrUpdate(new FixedMillisecond(current), population.getMinFitness());
		meanSolutionSeries.addOrUpdate(new FixedMillisecond(current), population.getMeanFitness());

		Runnable updater = new Runnable() {
			@Override
			public void run() {

//				update wykresu populacji i najlepszego dopasowania
				if(type==Type.TwoD) {
					populationSeries.clear();
					bestOfPopulationSeries.clear();

					for(aditz.logic.point.Point l : population.getAll()) {
						double x = l.getVal()[0];
						populationSeries.add(x, fit.fitnessFunc(new double[] { x}));
					}

					double fittestX = population.getFittest().getVal()[0];
					bestOfPopulationSeries.add(fittestX, fit.fitnessFunc(new double[] { fittestX}));
				}
				else if(type==Type.ThreeD) {
					p.chart3D.updatePopulation(population);
				}
				else {
					console.append(pt + " " + population.getMaxFitness() + "\n");
				}
			}
		};

		try {
			SwingUtilities.invokeAndWait(updater);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		// trochę wolniej
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void algorithmFinished(PopulationStats finalPopulation) {
		JOptionPane.showMessageDialog(null, result, "Result found", JOptionPane.INFORMATION_MESSAGE);

		p.chart3D.dispose();
		startButton.setText("Reset");
	}

	XYDataset getPopulationDataset() {
		populationSeries = new XYSeries("Population");

		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(populationSeries);

		return dataset;
	}

	XYDataset getBestOfPopulationDataset() {
		bestOfPopulationSeries = new XYSeries("Best of population");

		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(bestOfPopulationSeries);

		return dataset;
	}

	XYDataset getFunctionDataset() {
		functionDataset = DatasetUtilities.sampleFunction2D(
				new Function2D() {
					@Override
					public double getValue(double x) {
						return fit.fitnessFunc(new double[]{x});
					}
				},
				min,
				max,
				5000,
				"f(x)");

		return functionDataset;
	}

	public void setConsole(JTextArea console) {
		this.console = console;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setNow(long now) {
		this.now = now;
	}

	public void setFit(Fitness fit) {
		this.fit = fit;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}
}

