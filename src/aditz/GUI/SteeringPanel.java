package aditz.GUI;

import aditz.logic.crossover.ContinuousCrossover;
import aditz.logic.fitness.MaximumSeeking;
import aditz.logic.fitness.MinimumSeeking;
import aditz.logic.point.*;
import aditz.logic.selection.RankSelection;
import aditz.logic.selection.RouletteSelection;
import aditz.logic.selection.TournamentSelection;
import aditz.logic.stoping.NoChangeStop;
import aditz.logic.stoping.SimpleStopCondition;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:30
 */

class SteeringPanel extends JPanel implements ChangeListener {
	final JComboBox<String> method = new JComboBox<>(new String[] {"Maximum search", "Minimum search"});
	final JComboBox<String> crossingMethod = new JComboBox<>(new String[] {"Continous Crossover"});
	final JComboBox<String> pointEquality = new JComboBox<>(new String[] {"Exact point", "Loose point equality"});
	final JComboBox<String> stopCondition = new JComboBox<>(new String[] {"No change stop", "Simple stop"});
	final JComboBox<String> selectionMethod = new JComboBox<>(new String[] {"Rank Selection", "Rulette Selection", "TournamentTest Selection"});
	final JTextField mutatationProb = new JTextField("0.01");
	final JTextField populationCount = new JTextField("25");
	final JComboBox<String> elitismSet = new JComboBox<>(new String[] {"true", "false"});
	final JSlider slider = new JSlider(0, 700, 350);
	final JTextField selectionMethodCountMethod = new JTextField("15");
	final JTextField iterationsField = new JTextField("1000");
	final JTextField precisionField = new JTextField("10e-5");
	final JButton start = new JButton("Start");

	final JLabel methodLabel = new JLabel("Method: ");
	final JLabel crossingMethodLabel = new JLabel("Crossing method: ");
	final JLabel pointLabel = new JLabel("Point equality method: ");
	final JLabel stopLabel = new JLabel("Stop condition: ");
	final JLabel iterations = new JLabel("Iterations: ");
	final JLabel selectionLabel = new JLabel("Selection method: ");
	final JLabel selectionMethodCountLabel = new JLabel("Number of selected population: ");
	final JLabel mutationLabel = new JLabel("Mutation probability: ");
	final JLabel populationLabel = new JLabel("Population count: ");
	final JLabel elitismLabel = new JLabel("Elitism: ");
	final JLabel speedLabel = new JLabel("Speed: ");
	final JLabel precision = new JLabel("Precision: ");

	int count = 0;
	Panel panel;
	boolean isRunning = false;

	Thread run = new Thread(new Runnable() {
		@Override
		public void run() {
			panel.chart2D.setNow(new Date().getTime());
			panel.algo.startAlgorithm();
		}
	});


	SteeringPanel(final Panel panel) {
		super(new GridBagLayout());

		this.panel = panel;
		setBackground(Color.WHITE);

		slider.addChangeListener(this);
		slider.setInverted(true);
		slider.setBackground(Color.WHITE);

		add(methodLabel, new GBC(0, count, 2, 1));
		add(method, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(7));

		add(crossingMethodLabel, new GBC(0, count, 2, 1));
		add(crossingMethod, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(7));

		add(pointLabel, new GBC(0, count, 2, 1));
		add(pointEquality, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(7));

		add(stopLabel, new GBC(0, count, 2, 1));
		add(stopCondition, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setInset(7, 7, 0, 7));
		add(iterations, new GBC(0, count, 2, 1));
		add(iterationsField, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setInset(0, 7, 0, 7));
		add(precision, new GBC(0, count, 2, 1));
		add(precisionField, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setInset(0, 7, 7, 7));

		add(selectionLabel, new GBC(0, count, 2, 1));
		add(selectionMethod, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setInset(7, 7, 0, 7));
		add(selectionMethodCountLabel, new GBC(0, count, 2, 1));
		add(selectionMethodCountMethod, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setInset(0, 7, 7, 7));

		add(mutationLabel, new GBC(0, count, 2, 1));
		add(mutatationProb, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(7));

		add(populationLabel, new GBC(0, count, 2, 1));
		add(populationCount, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(7));

		add(elitismLabel, new GBC(0, count, 2, 1));
		add(elitismSet, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(7));

		add(speedLabel, new GBC(0, count, 2, 1));
		add(slider, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(7));

		add(start, new GBC(0, 100, 4, 1).setFill(GBC.BOTH).setWeight(100, 0).setIpad(0, 15).setAllInsets(7));
		panel.chart2D.startButton = start;

		stopCondition.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(stopCondition.getSelectedIndex()==0) {
					precision.setVisible(true);
					precisionField.setVisible(true);
					panel.revalidate();
				}
				else {
					precision.setVisible(false);
					precisionField.setVisible(false);
					panel.revalidate();
				}
			}
		});

		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(start.getText().equals("Start")) {
					setSettingsAlgo();
					run.start();

					start.setText("Interrupt");
					isRunning = true;
				}
				else if(start.getText().equals("Interrupt")) {
					panel.algo.interrupt();
					run.interrupt();
					start.setText("Reset");
				}
				else {
					panel.algo.interrupt();
					run.interrupt();
					panel.dispose();
				}
			}
		});
	}

	private void setSettingsAlgo() {
		double[] maxTable = new double[panel.hp.rp.labels.size()/2];
		double[] minTable = new double[panel.hp.rp.fields.size()/2];


		for(int i=0, j=0; j<panel.hp.rp.fields.size()-1; i++, j+=2) {
			minTable[i] = Double.parseDouble(panel.hp.rp.fields.get(j).getText());
			maxTable[i] = Double.parseDouble(panel.hp.rp.fields.get(j+1).getText());
		}

		aditz.logic.point.Point min=new aditz.logic.point.Point(minTable, panel.fit.fitnessFunc(minTable));
		aditz.logic.point.Point max=new aditz.logic.point.Point(maxTable, panel.fit.fitnessFunc(maxTable));

		if(method.getSelectedIndex()==0)
			panel.fit = new MaximumSeeking(panel.hp.functionText.getText());
		else
			panel.fit = new MinimumSeeking(panel.hp.functionText.getText());

		if(crossingMethod.getSelectedIndex()==0)
			panel.algo.setCrossover(new ContinuousCrossover(panel.fit,min,max));

		panel.algo.setFitness(panel.fit);

		if(pointEquality.getSelectedIndex()==0)
			panel.algo.setPointEquality(new ExactPointEquality());
		else
			panel.algo.setPointEquality(new LoosePointEquality());

		if(stopCondition.getSelectedIndex()==0)
			panel.algo.setStopCondition(new NoChangeStop(Integer.parseInt(iterationsField.getText()), Double.parseDouble(precisionField.getText())));
		else
			panel.algo.setStopCondition(new SimpleStopCondition(Integer.parseInt(iterationsField.getText())));

		if(selectionMethod.getSelectedIndex()==0)
			panel.algo.setSelectionMethod(new RankSelection(Integer.parseInt(selectionMethodCountMethod.getText())));
		else if(selectionMethod.getSelectedIndex()==1)
			panel.algo.setSelectionMethod(new RouletteSelection(Integer.parseInt(selectionMethodCountMethod.getText())));
		else
			panel.algo.setSelectionMethod(new TournamentSelection(Integer.parseInt(selectionMethodCountMethod.getText())));

		//settings
		panel.algo.setMinValue(min);
		panel.algo.setMaxValue(max);

		double prob = Double.parseDouble(mutatationProb.getText());
		panel.algo.setMutationProbability(prob);

		int sp = Integer.parseInt(populationCount.getText());
		panel.algo.setStartPopulation(sp);

		if(elitismSet.getSelectedIndex()==0)
			panel.algo.setElitism(true);
		else
			panel.algo.setElitism(false);

		panel.algo.setListener(panel.chart2D);

		if(!panel.algo.testBeforeGo())
			System.out.println("Not all properties were set");
	}

	public void stateChanged(ChangeEvent e) {
		panel.chart2D.setDelay(slider.getValue());
	}
}
