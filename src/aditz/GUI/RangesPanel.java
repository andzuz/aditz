package aditz.GUI;

import aditz.logic.point.*;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import javax.swing.*;
import java.awt.*;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:29
 */

class RangesPanel extends JPanel {
	Panel panel;
	ConsolePanel cp;
	int count = 0;

	JButton applyFunction = new JButton("Apply function");
	List<JLabel> labels = new LinkedList<>();
	List<JTextField> fields = new LinkedList<>();
	List<String> args;

	RangesPanel(List<String> args, Panel panel) {
		super(new GridBagLayout());

		this.args = args;
		this.panel = panel;
		setBackground(Color.WHITE);

		addFields();
		addButton();
	}

	private void addFields() {
		for(String s : args) {
			JLabel l1 = new JLabel(s + " min: ");
			JLabel l2 = new JLabel(s + " max: ");

			labels.add(l1);
			labels.add(l2);

			JTextField t1 = new JTextField("-40");
			JTextField t2 = new JTextField("40");

			fields.add(t1);
			fields.add(t2);

			add(l1, new GBC(0, count).setWeight(100, 0).setAllInsets(5).setAnchor(GBC.EAST));
			add(t1, new GBC(1, count).setWeight(100, 0).setFill(GBC.BOTH).setAllInsets(5));
			add(l2, new GBC(2, count).setWeight(100, 0).setAllInsets(5).setAnchor(GBC.EAST));
			add(t2, new GBC(3, count++).setWeight(100, 0).setFill(GBC.BOTH).setAllInsets(5));
		}
	}

	private void addButton() {
		add(applyFunction, new GBC(2, count++, 2, 1).setFill(GBC.BOTH).setAllInsets(5));

		applyFunction.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for(JTextField i : fields)
					i.setEnabled(false);

				panel.chart2D.setFit(panel.fit);

				if(panel.type==Type.TwoD) {
					panel.chart2D.setType(Type.TwoD);

					panel.chart2D.min = Double.parseDouble(panel.hp.rp.fields.get(0).getText());
					panel.chart2D.max = Double.parseDouble(panel.hp.rp.fields.get(1).getText());

					final JFreeChart chart2 = panel.chart2D.createFunctionChart(panel.chart2D.getBestOfPopulationDataset(), panel.chart2D.getPopulationDataset(), panel.chart2D.getFunctionDataset());
					final ChartPanel chartPanel2 = new ChartPanel(chart2);

					panel.chart2D.add(chartPanel2);
				}
				else if(panel.type==Type.ThreeD) {
					panel.chart2D.setType(Type.ThreeD);

					double[] maxTable = new double[panel.hp.rp.fields.size()/2];
					double[] minTable = new double[panel.hp.rp.fields.size()/2];


					for(int i=0, j=0; j<panel.hp.rp.fields.size()-1; i++, j+=2) {
						minTable[i] = Double.parseDouble(panel.hp.rp.fields.get(j).getText());
						maxTable[i] = Double.parseDouble(panel.hp.rp.fields.get(j+1).getText());
					}

					aditz.logic.point.Point min=new aditz.logic.point.Point(minTable, panel.fit.fitnessFunc(minTable));
					aditz.logic.point.Point max=new aditz.logic.point.Point(maxTable, panel.fit.fitnessFunc(maxTable));

					panel.chart3D.setFitness(panel.fit);
					panel.chart3D.setRange(min, max);
					panel.chart3D.initialize();

					panel.chart2D.add(panel.chart3D.getComp());
				}
				else {
					panel.chart2D.setType(Type.WhoKnows);
					cp = new ConsolePanel(panel);
					panel.chart2D.add(cp);
				}

				continueAdding();
				applyFunction.setVisible(false);
			}
		});
	}

	private void continueAdding() {
		add(new SteeringPanel(panel), new GBC(0, 4, 5, 1).setFill(GBC.BOTH).setWeight(100,100));
	}
}