package aditz.GUI;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:29
 */

class ConsolePanel extends JPanel {
	final JTextArea console = new JTextArea();
	final JScrollPane jsp = new JScrollPane(console);
	JLabel l = new JLabel("Console");

	Panel panel;

	ConsolePanel(Panel panel) {
		super(new GridBagLayout());

		this.panel = panel;
		panel.chart2D.setConsole(console);
		setBackground(Color.WHITE);

		l.setFont(new Font(l.getFont().getPSName(), Font.PLAIN, 24));
		DefaultCaret caret = (DefaultCaret) console.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		add(l, new GBC(0,0).setAllInsets(10));
		add(jsp, new GBC(0,1).setFill(GBC.BOTH).setWeight(100, 100).setAllInsets(10));
	}
}
