package aditz.GUI;

/**
 * Created by: Piotrek
 * Date: 28.05.13
 * Time: 22:30
 */

public enum Type {
	TwoD(2), ThreeD(3), WhoKnows(4);

	int dimension;

	Type(int d) { this.dimension = d; }
}