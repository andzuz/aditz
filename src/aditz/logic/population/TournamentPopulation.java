package aditz.logic.population;

import aditz.logic.fitness.Fitness;
import aditz.logic.point.Point;

import java.util.Collections;
import java.util.Random;

public class TournamentPopulation extends Population {
    private Random randomGenerator = new Random();
    public TournamentPopulation(int size) {
        super(size);
    }

    public void populateWithRandomFrom(Population src) {
        for(int i=0; i<size; i++)
            add(src.population.get(randomGenerator.nextInt(src.size)));
    }

    //stats
    @Override
    public double getMaxFitness() {
        return getFittest().getFitness();
    }

    @Override
    public double getMinFitness() {
        return Collections.min(population).getFitness();
    }

    @Override
    public Point getFittest() {
        return Collections.max(population);
    }
}
