package aditz.logic.population;

import aditz.logic.fitness.Fitness;
import aditz.logic.point.Point;

import java.util.*;

public class RoulettePopulation extends Population {
    private double roulette[];

    public RoulettePopulation(int size) {
        super(size);
    }

    public Point getByProbability(double probability) {
        if(roulette==null)
            buildRank();
        int index=Arrays.binarySearch(roulette, probability);
        if(index<0) index=-(index+1);
        return population.get(index);
    }

    public void populateWithBestFrom(Population src) {
        Collections.sort(src.population);
        for(int i=0; i<size; i++)
            add(src.population.get(src.size-i-1));    //from the bottom (best ones)
        buildRank();
    }

    private void buildRank() {
        roulette = new double[size];
        double minFitness=population.get(size-1).getFitness();
        double sumOfAllFitnesses=0; //normalized
        for(Point p: population)
            sumOfAllFitnesses+=p.getFitness()-minFitness;
        double sum=0;
        for(int i=0; i<size; i++) {
            sum+=(population.get(i).getFitness()-minFitness)/sumOfAllFitnesses;
            roulette[i]=sum;
        }
        roulette[size-1]=1;
    }

    @Override
    public double getMaxFitness() {
        return getFittest().getFitness();
    }

    @Override
    public double getMinFitness() {
        if(roulette!=null)
            return population.get(size-1).getFitness();
        return Collections.min(population).getFitness();
    }

    @Override
    public Point getFittest() {
        if(roulette!=null)
            return population.get(0);
        return Collections.max(population);
    }
}
