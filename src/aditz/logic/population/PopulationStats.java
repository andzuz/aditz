package aditz.logic.population;

import aditz.logic.point.Point;

import java.util.List;

/** represents population of solutions */
public interface PopulationStats {
    //use it to show stats
    double getMeanFitness();
    double getMaxFitness();
    double getMinFitness();

    //use it on graph
    List<Point> getAll();
    Point getFittest();
}
