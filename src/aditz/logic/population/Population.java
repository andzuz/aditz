package aditz.logic.population;

import aditz.logic.fitness.Fitness;
import aditz.logic.point.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public abstract class Population implements PopulationStats {
    protected List<Point> population;             //actual populatcion
    protected int size;                           //population size

    public Population(int size) {
        this.size=size;
        population = new ArrayList<>(size);
    }
    public void add(Point p) {
        population.add(p);
    }

    @Override
    public List<Point> getAll() {
        return Collections.unmodifiableList(population);
    }

    @Override
    public double getMeanFitness() {
        double result=0;
        for(Point p: population)
            result+=p.getFitness();
        return result/population.size();
    }

    //create random population (initial condition)
    private static Random random = new Random();
    public static void initWithRandom(Population population, Fitness fitness, Point min, Point max) {
        for(int i = 0; i < population.size; i++) {
            int dimension=min.getDimension();
            double point[]=new double[dimension];
            for(int j=0; j<dimension; j++) {
                double d = random.nextDouble();
                d*=(max.getVal()[j]-min.getVal()[j]);
                d+=min.getVal()[j]; //co za konstrukcja...
                point[j]=d;
            }
            population.add(new Point(point, fitness.fitnessFunc(point)));
        }
    }
}
