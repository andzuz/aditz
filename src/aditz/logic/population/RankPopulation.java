package aditz.logic.population;

import aditz.logic.fitness.Fitness;
import aditz.logic.point.Point;

import java.util.*;

public class RankPopulation extends Population {
    private int rank[];

    public RankPopulation(int size) {
        super(size);
    }

    public Point getByRank(int rankNo) {
        if(rank==null)
            buildRank();
        int index=Arrays.binarySearch(rank, rankNo); //rescale probability
        if(index<0) index=-(index+1);
        return population.get(index);
    }

    public void populateWithBestFrom(Population src) {
        Collections.sort(src.population);
        for(int i=0; i<size; i++)
            add(src.population.get(src.size-size+i));   //from the bottom, where the best are
        buildRank();
    }

    private void buildRank() {
        rank = new int[size];
        int sum=0;
        for(int i=1; i<=size; i++) {
            sum+=i;
            rank[i-1]=sum;
        }
    }

    public int getSumOfRanks() {
        return rank[size-1];
    }

    @Override
    public double getMaxFitness() {
        return getFittest().getFitness();
    }

    @Override
    public double getMinFitness() {
        if(rank!=null)
            return population.get(size-1).getFitness();
        return Collections.min(population).getFitness();
    }

    @Override
    public Point getFittest() {
        if(rank!=null)
            return population.get(0);
        return Collections.max(population);
    }
}
