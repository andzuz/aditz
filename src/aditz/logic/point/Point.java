package aditz.logic.point;

public class Point implements Comparable<Point>{
    private double values[];        //multidimension
    private double fitness;

    public Point(double[] val, double fitness) {   //what to do with fitness ?
        this.values = val;
        this.fitness = fitness;  //hmm... ?
    }

    public double[] getVal() {
        return values;
    }

    public double getFitness() {
        return fitness;
    }
    public int getDimension() {
        return values.length;
    }

    @Override
    public int compareTo(Point o) {     //default comparator for fitness comparing
        if(fitness < o.getFitness()) return -1;
        if(fitness > o.getFitness()) return 1;
        return 0;
    }
}
