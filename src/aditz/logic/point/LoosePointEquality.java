package aditz.logic.point;

public class LoosePointEquality implements PointEquality{
    @Override
    public boolean equal(Point left, Point right) {
        if(left.getDimension()!=right.getDimension())
            return false;
        for(int i=0; i<left.getDimension(); i++) {
            if(Double.compare(left.getVal()[i], right.getVal()[i])==0)  //it only takes one variable to determine equality
                return true;
        }
        return false;
    }
}
