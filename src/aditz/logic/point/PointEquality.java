package aditz.logic.point;

public interface PointEquality {
    public boolean equal(Point left, Point right);
}
