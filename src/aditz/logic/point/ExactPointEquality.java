package aditz.logic.point;

public class ExactPointEquality implements PointEquality {
    @Override
    public boolean equal(Point left, Point right) {
        if(left.getDimension()!=right.getDimension())
            return false;
        for(int i=0; i<left.getDimension(); i++) {
            if(Double.compare(left.getVal()[i], right.getVal()[i])!=0)  //the same point exactly
                return false;
        }
        return true;
    }
}
