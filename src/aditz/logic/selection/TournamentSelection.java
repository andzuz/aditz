package aditz.logic.selection;

import aditz.logic.point.Point;
import aditz.logic.population.Population;
import aditz.logic.population.TournamentPopulation;

public class TournamentSelection implements Selection {
    private int selected;
    private Population parents;

    public TournamentSelection(int selected) {
        this.selected = selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    @Override
    public void initSelection(Population population) {
        parents=population;
    }

    @Override
    public Population getPopulation(int size) {
        return new TournamentPopulation(size);
    }

    @Override
    public Point selectParent() {
        TournamentPopulation temp=new TournamentPopulation(selected);
        temp.populateWithRandomFrom(parents);
        return temp.getFittest();
    }
}
