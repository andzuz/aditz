package aditz.logic.selection;

import aditz.logic.point.Point;
import aditz.logic.population.Population;
import aditz.logic.population.RoulettePopulation;

import java.util.Random;

public class RouletteSelection implements Selection {
    private int selected;
    private RoulettePopulation parents;
    private Random rand=new Random();

    public RouletteSelection(int selected) {
        this.selected = selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    @Override
    public void initSelection(Population population) {
        RoulettePopulation rp=new RoulettePopulation(selected);
        rp.populateWithBestFrom(population);
        parents=rp;
    }

    @Override
    public Population getPopulation(int size) {
        return new RoulettePopulation(size);
    }

    @Override
    public Point selectParent() {
        double prob=rand.nextDouble();
        return parents.getByProbability(prob);
    }
}
