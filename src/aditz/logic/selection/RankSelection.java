package aditz.logic.selection;

import aditz.logic.point.Point;
import aditz.logic.population.Population;
import aditz.logic.population.RankPopulation;

import java.util.Random;

public class RankSelection implements Selection {
    private int selected;
    private RankPopulation parents;
    private Random rand=new Random();

    public RankSelection(int selected) {
        this.selected = selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    @Override
    public void initSelection(Population population) {
        RankPopulation rp=new RankPopulation(selected);
        rp.populateWithBestFrom(population);
        parents=rp;
    }

    @Override
    public Population getPopulation(int size) {
        return new RankPopulation(size);
    }

    @Override
    public Point selectParent() {
        int rank=rand.nextInt(parents.getSumOfRanks());
        return parents.getByRank(rank);
    }
}
