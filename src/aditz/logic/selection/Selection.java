package aditz.logic.selection;

import aditz.logic.fitness.Fitness;
import aditz.logic.point.Point;
import aditz.logic.population.Population;

public interface Selection {
    void initSelection(Population population); //init selection
    Population getPopulation(int size);     //factory
    Point selectParent();           //actual selectiong
}
