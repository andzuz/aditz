package aditz.logic.tests;

import aditz.logic.algorithm.Algorithm;
import aditz.logic.crossover.ContinuousCrossover;
import aditz.logic.fitness.Fitness;
import aditz.logic.fitness.MaximumSeeking;
import aditz.logic.fitness.MinimumSeeking;
import aditz.logic.point.ExactPointEquality;
import aditz.logic.point.LoosePointEquality;
import aditz.logic.point.Point;
import aditz.logic.selection.RankSelection;
import aditz.logic.selection.RouletteSelection;
import aditz.logic.stoping.NoChangeStop;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class RankTest {
    @Test
    public void test() {
        Algorithm algo = new Algorithm();
        Fitness func = new MinimumSeeking("x^2+(x+y)^2+1/(x^2+y^2+1)");

        //assertEquals(func.fitnessFunc(100, 90, 10), -100*90*10/Math.PI);

        Point min=new Point(new double[] {-1000000,-1000000}, func.fitnessFunc(new double[] {-1000000,-1000000}));
        Point max=new Point(new double[] { 1000000, 1000000}, func.fitnessFunc(new double[] { 1000000, 1000000}));

        //Inject Properties and Dependencies
        //dependencies
        algo.setCrossover(new ContinuousCrossover(func,min,max));
        algo.setFitness(func);

        algo.setPointEquality(new LoosePointEquality());
        algo.setStopCondition(new NoChangeStop(10, 10e-5));
        algo.setSelectionMethod(new RankSelection(15));

        //settings
        algo.setMinValue(min);
        algo.setMaxValue(max);
        algo.setMutationProbability(0.01);
        algo.setStartPopulation(20);
        algo.setElitism(true);
        algo.setListener(new Reporter());

        if(algo.testBeforeGo())
            algo.startAlgorithm();
        else
            System.out.println("Not all properties were set");
    }
}
