package aditz.logic.tests;

import aditz.logic.algorithm.Algorithm;
import aditz.logic.algorithm.AlgorithmProgressListener;
import aditz.logic.crossover.ContinuousCrossover;
import aditz.logic.fitness.Fitness;
import aditz.logic.fitness.MaximumSeeking;
import aditz.logic.point.ExactPointEquality;
import aditz.logic.point.LoosePointEquality;
import aditz.logic.point.Point;
import aditz.logic.population.PopulationStats;
import aditz.logic.selection.RouletteSelection;
import aditz.logic.selection.TournamentSelection;
import aditz.logic.stoping.NoChangeStop;
import org.junit.Test;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;

public class TournamentTest {
    @Test
    public void test() {
        Algorithm algo = new Algorithm();
        Fitness func = new MaximumSeeking("1/x");

        //assertEquals(func.fitnessFunc(100, 90, 10), 100*90*10/Math.PI);


        Point min=new Point(new double[] {-10}, func.fitnessFunc(new double[] {-10}));
        Point max=new Point(new double[] { 10}, func.fitnessFunc(new double[] { 10}));

        //Inject Properties and Dependencies
        //dependencies
        algo.setCrossover(new ContinuousCrossover(func,min,max));
        algo.setFitness(func);

        algo.setPointEquality(new LoosePointEquality());
        algo.setStopCondition(new NoChangeStop(10, 10e-5));
        algo.setSelectionMethod(new TournamentSelection(5));

        //settings
        algo.setMinValue(min);
        algo.setMaxValue(max);
        algo.setMutationProbability(0.01);
        algo.setStartPopulation(20);
        algo.setElitism(true);
        algo.setListener(new Reporter());

        if(algo.testBeforeGo())
            algo.startAlgorithm();
        else
            System.out.println("Not all properties were set");
    }
}

class Reporter implements AlgorithmProgressListener {
    @Override
    public void newGenerationCreated(PopulationStats population) {
        String pt= Arrays.toString(population.getFittest().getVal());
        System.out.println(pt + " " + population.getMaxFitness());
    }

    @Override
    public void algorithmFinished(PopulationStats finalPopulation) {
        System.out.println("DONE");
    }
}