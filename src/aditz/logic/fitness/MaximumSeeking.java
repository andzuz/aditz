package aditz.logic.fitness;

import aditz.parser.Parser;
import aditz.parser.exceptions.ParserException;

public class MaximumSeeking implements Fitness {
	Parser parser=new Parser();

    public MaximumSeeking(String expression) {
        try {
            parser.parse(expression);
        } catch (ParserException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

	@Override
	public Parser getParser() {
		return parser;
	}

    @Override
    public double fitnessFunc(double... d) {
        return parser.fitnessFunc(d);
    }

}
