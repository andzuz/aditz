package aditz.logic.fitness;

import aditz.parser.Parser;

public interface Fitness {
	public Parser getParser();
    public double fitnessFunc(double[] d);
}
