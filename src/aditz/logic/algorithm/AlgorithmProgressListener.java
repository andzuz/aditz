package aditz.logic.algorithm;

import aditz.logic.population.PopulationStats;

/* to use in aditz.GUI.GUI, may be extended with additional methods
   ex. to inform about mutation, crossover, whatever    */

public interface AlgorithmProgressListener {
    void newGenerationCreated(PopulationStats population);
    void algorithmFinished(PopulationStats finalPopulation);
}
