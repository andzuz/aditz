package aditz.logic.algorithm;

import aditz.logic.crossover.Crossover;
import aditz.logic.fitness.Fitness;
import aditz.logic.point.Point;
import aditz.logic.point.PointEquality;
import aditz.logic.population.Population;
import aditz.logic.stoping.StopCondition;
import aditz.logic.selection.Selection;


import java.util.Random;

public class Algorithm {
    private Random rand=new Random();
    private final int fault=10;

    //Changable strategies
    private Crossover crossover;
    private Fitness fitness;
    private Selection selectionMethod;
    private PointEquality pointEquality;
    private StopCondition stopCondition;

    /** properties to use in Algoritm */
    private int startPopulation;
    private double mutationProbability;
    private Point minValue;     //constrains
    private Point maxValue;
    private boolean elitism;
	private boolean interrupt = false;


    private AlgorithmProgressListener listener;

    //setters
    public void setCrossover(Crossover cross) { crossover = cross; }
    public void setFitness(Fitness fitness) { this.fitness = fitness; }
    public void setStopCondition(StopCondition stopCondition) { this.stopCondition = stopCondition; }
    public void setSelectionMethod(Selection selectionMethod) { this.selectionMethod = selectionMethod; }
    public void setPointEquality(PointEquality pointEquality) { this.pointEquality = pointEquality; }

    public void setMaxValue(Point maxValue) { this.maxValue = maxValue; }
    public void setMinValue(Point minValue) { this.minValue = minValue; }
    public void setMutationProbability(double mutationProbability) { this.mutationProbability = mutationProbability; }
    public void setStartPopulation(int startPopulation) { this.startPopulation = startPopulation; }
    public void setElitism(boolean elitism) { this.elitism=elitism; }

    public void setListener(AlgorithmProgressListener listener) { this.listener=listener; }

    public Algorithm() {} //no point in making any args for constructor, if you dont initialize you get NPE bitch!

    private void report(Population population) {
        if(listener!=null)
            listener.newGenerationCreated(population);
    }

    public boolean testBeforeGo() {
        if(crossover==null || fitness==null || selectionMethod==null || pointEquality==null || stopCondition==null)
            return false;
        if(startPopulation==0 || mutationProbability==0 || minValue==null || maxValue==null)
            return false;
        return true;
    }

    //algorithm itself
    public void startAlgorithm() {
        Population population=selectionMethod.getPopulation(startPopulation);
        Population.initWithRandom(population,fitness, minValue,maxValue);
        report(population);
	    interrupt = false;

        while(!stopCondition.isFinished(population) & !interrupt) {
            Population nextGen = selectionMethod.getPopulation(startPopulation);

            int j=0;
            if(elitism) { //if elitism enabled
                j++;
                nextGen.add(population.getFittest());
            }
            //populate it with new species
            for(; j < startPopulation; j++) {
                selectionMethod.initSelection(population);
                Point mother, father;   //a nie jakiś tam rodzic_jeden, rodzic_dwa !
                int i=0;
                do {
                    mother = selectionMethod.selectParent();
                    father = selectionMethod.selectParent();
                    i++;
                } while(i<fault && pointEquality.equal(father,mother));  //not to select two same elements, add fault mechanism

                Point child = crossover.cross(mother, father); //perform crossover
                if(rand.nextDouble() < mutationProbability) { //mutation
                    child = crossover.mutate(child);
                }
                nextGen.add(child); //add new offspring to population
            }
            population=nextGen;
            report(population);
        }

        if(listener!=null)
            listener.algorithmFinished(population);
    }

	public void interrupt() {
		interrupt = true;
	}
}
