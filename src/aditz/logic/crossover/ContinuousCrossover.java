package aditz.logic.crossover;

import aditz.logic.fitness.Fitness;
import aditz.logic.point.Point;

import java.util.Random;

public class ContinuousCrossover implements Crossover {
    private Random random=new Random();
    private Point min, max;
    private Fitness fitness;

    public ContinuousCrossover(Fitness func, Point min, Point max) {
        this.fitness=func;
        this.max=max;
        this.min=min;
    }

    @Override
    public Point cross(Point l, Point r) {
        double[] newPoint=new double[l.getDimension()];
        int no=random.nextInt(l.getDimension());
        for(int i=0; i<l.getDimension(); i++) {
            do {
                if(i==no) {
                    double beta=2*random.nextDouble()-1;  //-1 to 1
                    beta*=2;
                    newPoint[i]=r.getVal()[i]+beta*(l.getVal()[i]-r.getVal()[i]);
                }
                else {
                    if(random.nextBoolean())
                        newPoint[i]=l.getVal()[i];
                    else
                        newPoint[i]=r.getVal()[i];
                }
            } while (newPoint[i]<min.getVal()[i] || newPoint[i]>max.getVal()[i]);
        }
        return new Point(newPoint,fitness.fitnessFunc(newPoint));
    }

    @Override
    public Point mutate(Point m) {      //Fixed
        int no=random.nextInt(m.getDimension());
        do {
            m.getVal()[no]=m.getVal()[no]+random.nextGaussian()*(max.getVal()[no]-min.getVal()[no]);
        } while (m.getVal()[no]<min.getVal()[no] || m.getVal()[no]>max.getVal()[no]);
        return new Point(m.getVal(), fitness.fitnessFunc(m.getVal()));
    }
}
