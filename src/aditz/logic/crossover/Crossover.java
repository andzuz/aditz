package aditz.logic.crossover;

import aditz.logic.point.Point;

public interface Crossover {
    Point cross(Point l, Point r);
    Point mutate(Point m);
}
