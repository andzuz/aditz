package aditz.logic.stoping;

import aditz.logic.population.PopulationStats;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 21.05.13
 * Time: 12:49
 * To change this template use File | Settings | File Templates.
 */
public interface StopCondition {
    boolean isFinished(PopulationStats population);
}
