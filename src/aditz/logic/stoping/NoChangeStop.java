package aditz.logic.stoping;

import aditz.logic.point.Point;
import aditz.logic.population.PopulationStats;

public class NoChangeStop implements StopCondition {
    private final int noChangeIter;
    private final double eps;
    private int currentIter=0;
    private Point bestSolution=new Point(null, Double.MIN_VALUE);

    public NoChangeStop(int noChangeIter, double eps) {
        this.noChangeIter = noChangeIter;
        this.eps=eps;
    }

    @Override
    public boolean isFinished(PopulationStats population) {
        Point populationBest=population.getFittest();
        if(Math.abs(populationBest.getFitness() - bestSolution.getFitness())<eps)
            return noChangeIter==currentIter++;
        else {
            bestSolution=populationBest;
            currentIter=0;
            return false;
        }
    }
}
