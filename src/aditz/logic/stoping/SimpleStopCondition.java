package aditz.logic.stoping;

import aditz.logic.population.PopulationStats;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 21.05.13
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
public class SimpleStopCondition implements StopCondition {
    private int iter;
    private final int maxIter;
    public SimpleStopCondition(int maxIter) {
        this.maxIter=maxIter;
    }
    @Override
    public boolean isFinished(PopulationStats population) {
        return maxIter==iter++;
    }
}
