Co dodane i jak to tera działa

1)  Metody selekcji, są trzy Ruletkowa, Turnamentowa Rankingowa każda wspierana swoją własną Populacją (przechowującą
    odpowiednio tablice do zapisywania prawdopodobieństwa i rankingu oraz własne metody specyficzne dla danej selekcji)
    każda z metod wymaga podania w konstuktorze selectNumber który oznacza z jak wielkiej kolekcji mają być wybierani rodzice
    np w turnamentowej ta liczba powinna być mała, a w ruletkowej/rankingowej stosunkowo duża w odniesieniu do całkowitej
    populacji. Turnamentowa np za każdym razem gdy ma zwrócić rodzica korzysta z selectNumber aby stworzyć nową kolekcjie
    o właśnie tym rozmiarze i z losowymi elementami, po czym zwrócić najlepszy. Ruletkowa i rankingowa tworzy nową kolekcje
    raz dla każdej iteracji (buduje też tablice losowania) po czym zwraca wylosowany element z tej kolekcji (z odpowienim
    rozkładem przy losowaniu). selectNumber można zmienić setterem podczas działania

2)  Metody zatrzymania, dwie
        pierwsza SimpleStopCondition, podajesz ilość iteracji w konstruktorze i tyle
        druga NoChangeStop, podajesz ilość iteracji i dokładność jeśli po podanej ilości iteracji dokładność zmian nie
            przekroczy zadanej to algorytm się kończy

3)  Algorytm jest jeden uniwersalny, do niego przez settery wprowadza sie ustawienia, parametry i strategie (biedne DI)
    dodana metoda testBeforeGo coby sprawdzić przed uruchomieniem czy wszystko podane jak należy

4)  Metody porównywania punktów, też są dosyć istotne
         ExactPointEquality wszystkie współrzędne muszą być równe aby orzec równość punktów
         LoosePointEquality wystarczy jedna taka sama współrzędna i punkty uznawane za takie same (czasem mocno
         przyspiesza działanie)

TODO:
    błędy numeryczne, dzielenie przez zero, NaN, te rzeczy w łatwy sposób mogą rozjebać algorytm